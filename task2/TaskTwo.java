package com.company.task2;

import java.util.Arrays;
import java.util.Scanner;

public class TaskTwo {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        double[] array = {1, 2, 3, 4, 5};
        System.out.println("Какой элемент увеличить:" + Arrays.toString(array));
        int element = scanner.nextInt() - 1;
        newArr(element, array);
        System.out.println(Arrays.toString(array));
    }

    private static void newArr(int element, double[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i == element) {
                array[i] += array[i] * 0.1;
            }
        }
    }
}
