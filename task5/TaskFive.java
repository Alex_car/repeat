package com.company.task5;

public class TaskFive {

    public static void main(String[] args) {
        System.out.println("Простые числа:");
        for (int i = 2; i < 101; i++) {
            int cout = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    cout++;
                }
            }
            if (cout <= 2) {
                System.out.print(i + " ");
            }
        }
    }
}
