package com.company.task7;

import java.util.Scanner;

public class TaskSeven {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double numb = scanner.nextDouble();

        if (numb%1==0){
            System.out.println("Число целое");
        } else {
            System.out.println("Число дробное");
        }
    }
}
