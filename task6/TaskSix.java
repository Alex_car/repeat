package com.company.task6;

import java.util.Scanner;

public class TaskSix {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число для умножения: ");
        int numb = scanner.nextInt();

        for (int i = 1; i < 11; i++) {
            System.out.println(numb + " * " + i + " = " + numb * i);
        }
    }
}
