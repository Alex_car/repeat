package com.company.task13;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskTherteen {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<String> line = new ArrayList<>();

        System.out.print("Введите один символ: ");
        line.add(scanner.nextLine());

        if (line.size() > 1) {
            System.out.println("Вводить надо один элемент");
            return;
        }

        char c = line.get(0).charAt(0);

        if (Character.isDigit(c)) System.out.println(c + " - Это цифра");
        if (Character.isLetter(c)) System.out.println(c + " - Это буква");
        if (line.contains(".,:;-")) System.out.println(c + " - Это знак пунктуации");
        if (line.contains(" ")) System.out.println(c + " - Это пробел");
    }
}
