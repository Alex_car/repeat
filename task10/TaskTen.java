package com.company.task10;

import java.util.Scanner;

public class TaskTen {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        String line = scanner.nextLine();

        if (isPalendrom(line)) {
            System.out.println("Строка палиндром");
        } else {
            System.out.println("Строка не палиндром");
        }
    }

    private static boolean isPalendrom(String line) {
        String workLineOne = line.replaceAll("[\\s()?:!.,;{}\"-]", "");
        String workLineTwo = new StringBuffer(workLineOne).reverse().toString();
        boolean retVal = workLineOne.equalsIgnoreCase(workLineTwo);
        return retVal;
    }
}
