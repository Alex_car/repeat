package com.company.task11;

import java.util.Scanner;

public class TaskEleven {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите количество суток: ");
        int days = scanner.nextInt();
        System.out.println("Часов: "+days*24+
                "\nМинут: "+days*24*60+
                "\nСекунд: "+days*24*60*60);
    }
}
