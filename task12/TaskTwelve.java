package com.company.task12;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Scanner;

public class TaskTwelve {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        int[][] matrix = new int[line][column];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }

        for (int i = 0; i < line; i++, System.out.println()) {
            for (int j = 0; j < column; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }

        ArrayList<Integer> massive = new ArrayList<>();
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                massive.add(matrix[i][j]);
            }
        }

        int arrListSize = massive.size();
        System.out.println("Одномерный массив: ");
        for (int i = 0; i < arrListSize; i++) {
            if (i % column == 0 && i != 0) {
                System.out.print(" ");
            }
            System.out.print(massive.get(i));
        }

       // System.out.println("\n"+Arrays.deepToString(matrix));
    }
}
