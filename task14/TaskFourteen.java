package com.company.task14;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskFourteen {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        int[][] matrix = new int[line][column];

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }

        soutMatrix(line, column, matrix);

        int[][] matrixTrans = new int[column][line];
        ArrayList<Integer> lol = new ArrayList<>();

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                lol.add(matrix[i][j]);
            }
        }

        int k = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                matrixTrans[j][i] = lol.get(k);
                k++;
            }
        }

        System.out.println("Транспанированная матрица:");
        soutMatrix(column, line, matrixTrans);
    }

    public static void soutMatrix(int line, int column, int[][] matrix) {
        for (int i = 0; i < line; i++, System.out.println()) {
            for (int j = 0; j < column; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }

}

