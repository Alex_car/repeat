package com.company.task3;

import java.util.Scanner;

public class TaskTree {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Рубли: ");
        double rubles = scanner.nextDouble();
        System.out.print("Курс евро: ");
        double rate = scanner.nextDouble();
        System.out.printf("%.3f рублей = %.3f евро",rubles,euro(rate,rubles));
    }

    private static double euro(double rate, double rubles) {
        return rubles / rate;
    }
}
