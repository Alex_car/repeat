package com.company.task1;

import java.util.Scanner;

public class TaskOne {
  public static   Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите строку с точкой:");
        String text = scanner.nextLine();

        if (!text.contains(".")) {
            System.out.println("Нет точки.");
            return;
        }
        String output = text.substring(0, text.indexOf('.'));

        int length = text.indexOf(".");

        output = output.replaceAll(" ", "");
        int q = length - output.length();

        System.out.print("Количество символов до точки:" + length);
        System.out.print("\nКоличество пробелов:" + q);
    }
}
